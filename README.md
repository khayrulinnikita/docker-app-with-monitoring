Запуск приложения:

git clone https://gitlab.com/khayrulinnikita/docker-app-with-monitoring.git

cd monitoring/

ADMIN_USER=admin ADMIN_PASSWORD=admin docker-compose up -d

Предварительно должен быть установлен docker и docker-compose

    После загрузки и запуска образов приложение будет доступно по адресу http://<host_ip>
    Graphana доступна по адресу http://<host_ip>:3000 (user admin password admin, можно сменить при входе)

Приложение состоит из двух контейнеров, собранных на базе образов alpina. Одним образом является nginx(можно было использовать готовый образ nginx'a с официального репозитория на dockerhub), вторым - hello-world приложение, написаное на python с использованием фреймворка flask.

Graphana Dashboard содержит в себе dashboard'ы с монитором обоих контейнеров(nginx и flask) и монитор самого хоста.
